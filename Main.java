import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Введите : ");
        String input = sc.nextLine();
        String[] lines = input.split("\n");
        int count = 0;


        if(input.length() != 0){
            count++;
            for (int i = 0; i < input.length(); i++) {
                if(input.charAt(i) == ' '){
                    count++;
                }
            }
        }
        System.out.println(Arrays.asList(lines));
        System.out.println("Количество слов: "+count);
    }
}